/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package ru.ael.lesson6operators;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author developer
 */
public class Lesson6Operators {

    public static void main(String[] args) {
        System.out.println("Операторы отношения и логические операторы");

        int a = 5;
        int b = 100;

        
        // Оператор a > b возвращает логическое значение true (истина) или false (ложь)
        if (a > b) {
            // код, выполняющийся когда a > b ( т.е.  a > b истинно)
            System.out.println("а = [" + a + "] больше b = [" + a + "] - УСЛОВИЕ ВЫПОЛНЯЕСЯ ");
        } else {
            // код, выполняющийся когда условие  a > b ложно
            System.out.println("а = [" + a + "] больше b = [" + a + "] - УСЛОВИЕ НЕ ВЫПОЛНЯЕСЯ ");
        }

        
        System.out.println("Проверка константы true");
        if (true) {
            System.out.println("а = [" + a + "] больше b = [" + a + "] - УСЛОВИЕ ВЫПОЛНЯЕСЯ ");
        } else {
            System.out.println("а = [" + a + "] больше b = [" + a + "] - УСЛОВИЕ НЕ ВЫПОЛНЯЕСЯ ");
        }
        
          
        System.out.println("Проверка константы fasle");
        if (false) {
            System.out.println("а = [" + a + "] больше b = [" + a + "] - УСЛОВИЕ ВЫПОЛНЯЕСЯ ");
        } else {
            System.out.println("а = [" + a + "] больше b = [" + a + "] - УСЛОВИЕ НЕ ВЫПОЛНЯЕСЯ ");
        }
        
        System.out.println("Проверка содержимого массива целых чисел");
        
        List<Integer> listA = new ArrayList<>(); // пустой массив чисел listA
        List<Integer> listB = new ArrayList<>(); // пустой массив чисел listB
        List<Integer> listС = new ArrayList<>(); // пустой массив чисел listB
        System.out.println("Количество элементов массива до добавления = "+listA.size());
        boolean empty = listA.isEmpty();
        System.out.println("Массив пустой?  = " + empty);
        
        
        listA.toString();        
        System.out.println("Массив целых чисел исходный: " + listA.toString());       
        
        // Добавление в массив элементов
        listA.add(a);
        listA.add(b);
        
        empty = listA.isEmpty();
        System.out.println("Массив целых чисел после добавления a,b: " + listA);       
        System.out.println("Количество элементов массива после добавления a,b = "+listA.size());
        System.out.println("Массив пустой?  = " + empty);
        
        listA.add(75); // добавление константы 75
        
        System.out.println("Массив целых чисел после добавления константы 75: " + listA);       
        
        System.out.println("Проверка размера массива >= 3");
        if (listA.size() >= 3) {
            boolean result = true;
            System.out.println("Размер массива >=3 ДА [" + result + "]");
        } else {
            boolean result = false;
            System.out.println("Размер массива >=3 НЕТ [" + result + "]");
        }
        
        System.out.println("Проверка размера массива >= 4");
        if (listA.size() >= 4) {
            boolean result = true;
            System.out.println("Размер массива >=3 ДА [" + result + "]");
        } else {
            boolean result = false;
            System.out.println("Размер массива >=3 НЕТ [" + result + "]");
        }
        
               System.out.println("Множественные операторы отношений");
        // && - логическом умножение (И)
        // если массив listA не пустой и массив listB не пустой, то ...
        // знак ! инвертирует логичекое выражение
        if (!listA.isEmpty() && !listB.isEmpty()) {
            System.out.println("Массивы не пустые - выполяю проверку размеров массивов ...");
        } else {
            System.out.println("Массивы не пустые - выполяю проверку размеров массивов ...");
        }

        
        
    }
}
